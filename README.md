# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone
```

Naloga 6.2.3:

```
git add jscolor.js
git rm nepotrebno.js

git commit -m "Priprava potrebnih JavaScript knjižnic"

((UVELJAVITEV))
https://bitbucket.org/urban120/stroboskop2/commits/e26e243d3ea5a0e92eba775c3fef27af28551ce8
```

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:

```
git branch izgled
git checkout izgled

git commit -a -m "Dodajanje stilov spletne strani"

((UVELJAVITEV))
https://bitbucket.org/urban120/stroboskop2/commits/bd9be42fbb19efd9386070a934bc78883d5ed85a
```

Naloga 6.3.2:

```
git commit -a -m "Desna poravnava besedil"

((UVELJAVITEV))
https://bitbucket.org/urban120/stroboskop2/commits/0e7d7d73b9d955a8a95aeae6489791f4113baec2
```

Naloga 6.3.3:

```
git commit -a -m "Dodajanje gumba za dodajanje barv"

((UVELJAVITEV))
https://bitbucket.org/urban120/stroboskop2/commits/1ed46a5519f83260f72f937f5384a5abade9472d
```

Naloga 6.3.4:

```
git commit -a -m "Dodajanje robov in senc"

((UVELJAVITEV))
https://bitbucket.org/urban120/stroboskop2/commits/6317c9aa0d48e36cae6f43a7d518bf7c23d21d2a
```

Naloga 6.3.5:

```
git push
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:

```
git branch dinamika
git checkout master

git commit -a -m "Dodajanje izbiranja barv"

((UVELJAVITEV))
https://bitbucket.org/urban120/stroboskop2/commits/e9eb0006e7cce87e2d2f32a0a248372f52113a12
```

Naloga 6.4.2:

```
git commit -a -m "Dinamika gumba za dodajanje barv"

((UVELJAVITEV))
https://bitbucket.org/urban120/stroboskop2/commits/96d4b80650c72e1faaad815164eb796df0afbd3c
```

Ugotovil sem da imam narobe gumba dodaj in odstrani barvo
Zato sem se odločil da jih bom popravil in dodal v master

```
git checkout master
git commit -a -m "sprememba izgleda gombov dodaj in odstani barve"

git checkout dinamika
```

Nov commit bom dodal v vejo dinamika

```
git merge master
git push
```

Naloga 6.4.3:

```
git commit -a -m "Dinamika gumba za ustavitev stroboskopa"

((UVELJAVITEV))
https://bitbucket.org/urban120/stroboskop2/commits/887b7934ff6bdcf14a95150f3742841d098431d8
```

Naloga 6.4.4:

```
git commit -a -m "Dinamika gumba za zagon stroboskopa"

((UVELJAVITEV))
https://bitbucket.org/urban120/stroboskop2/commits/30071245832bd413cffbfe88ba21af0deb36e66d
```

Naloga 6.4.5

```
git checkout master
git merge dinamika
```

Urejanje README.md

```
git commit -a -m "Urejanje README.md"
```

